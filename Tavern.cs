using System.Collections.Generic;

namespace TestApp
{
    public class Tavern
    {
        private int _gold;
        private int _beerPrice = 3;

        public void SellBeerTo(IPayable chr)
        {
            _gold += chr.Pay(_beerPrice);
        }
    }
}