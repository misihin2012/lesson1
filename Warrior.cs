using System;

namespace TestApp
{
    public class Warrior : Character
    {
        public Warrior()
        {
            _hp = 120;
            _mp = 30;
            _wp = 100;

            _strength = 3;
            _agility = 2;
            _intelligence = 1;

            _damage = 12;
            _defense = 4;
        }

        public override int Attack()
        {
            return _damage * _strength;
        }

        public override void Info()
        {
            Console.WriteLine("Воин:");
            base.Info();
        }
    }
}