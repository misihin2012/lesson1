﻿using System;

namespace TestApp
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            Archer archer = new Archer();
            Warrior warrior = new Warrior();
            Wizard wizard = new Wizard();
            
            archer.DamagedBy(warrior);

            Tavern tavern = new Tavern();

            tavern.SellBeerTo(warrior);
            tavern.SellBeerTo(archer);
            tavern.SellBeerTo(wizard);

            archer.Info();
            warrior.Info();
            wizard.Info();
        }
    }
}
