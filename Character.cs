using System;

namespace TestApp
{
    public abstract class Character : IPayable
    {
        protected int _hp;
        protected int _mp;
        protected int _wp;
        
        protected int _strength;
        protected int _agility;
        protected int _intelligence;

        protected int _damage;
        protected int _defense;

        protected int _gold = 10;

        public abstract int Attack();

        public void DamagedBy(Character chr)
        {
            _hp -= (chr.Attack() - _defense);
        }

        public int Pay(int price)
        {
            if (_gold < price)
            {
                return 0;
            }

            _gold -= price;
            return price;
        }

        public virtual void Info()
        {
            string text = "Сила: {0}   ";
            text += "Ловкость: {1}   ";
            text += "Интеллект: {2}\n";
            text += "Атака: {3}   ";
            text += "Защита: {4}\n";
            text += "HP: {5}   ";
            text += "MP: {6}   ";
            text += "WP: {7}\n";
            text += "Золото: {8}\n";

            string output = String.Format(
                text,
                _strength,
                _agility,
                _intelligence,
                _damage,
                _defense,
                _hp,
                _mp,
                _wp,
                _gold
            );
            
            Console.WriteLine(output);
        }
    }
}