namespace TestApp
{
    public interface IPayable
    {
        public int Pay(int price);
    }
}