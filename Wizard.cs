using System;

namespace TestApp
{
    public class Wizard : Character
    {
        public Wizard()
        {
            _hp = 80;
            _mp = 100;
            _wp = 60;

            _strength = 1;
            _agility = 2;
            _intelligence = 3;

            _damage = 10;
            _defense = 2;
        }

        public override int Attack()
        {
            return _damage * _intelligence;
        }

        public override void Info()
        {
            Console.WriteLine("Маг:");
            base.Info();
        }
    }
}