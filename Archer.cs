using System;

namespace TestApp
{
    public class Archer : Character
    {
        public Archer()
        {
            _hp = 100;
            _mp = 50;
            _wp = 80;
            
            _strength = 1;
            _agility = 3;
            _intelligence = 2;

            _damage = 10;
            _defense = 3;
        }

        public override int Attack()
        {
            return _damage * _agility;
        }

        public override void Info()
        {
            Console.WriteLine("Лучник:");
            base.Info();
        }
    }
}